**Meeting 11/18**

*Ben:*

* Accomplishments: Worked  through a couple graphQl tutorials and got a basic boilerplate thing set up. Going to add a couple more testing and data loader features. And probably some mail wares for authentication. 
* Concerns: None
* Roadblocks: None

*Joey:*

* Accomplishments: We finished the UI prototype and added Jims other elements needed (small fixes)
* Concerns: None
* Roadblocks:

*Anna:*

* Accomplishments: We finished the UI prototype and added Jims other elements needed (small fixes) and drafted up a message to John Sweet inquiring about which license would fit our project the best
* Concerns: Just I feel like this week has been busy, concerned about learning react… (personal) not big with the works.
* Roadblocks: None

**Meeting 11/20:**

*Ben:*

* Accomplishments: Added to the UX tools part for the design review, and Presented Design Review! Added graphql service boilerplate over the last couple of days. Making the graphql run faster with cacheing. 
* Concerns: None
* Roadblocks: None

*Joey:*

* Accomplishments: Finished the design review presentation and presented.
* Concerns:
* Roadblocks:

*Anna:*

* Accomplishments: Presented Design Review!, going to go through react tutorial and attempt log in page.
* Concerns: None
* Roadblocks: None

**Meeting 11/23**

*Ben:*

* Accomplishments: Not really worked on capstone this weekend but continued worked on GraphQL stuff
* Concerns: None
* Roadblocks: None

*Joey:*

* Accomplishments: Worked on the React.js tutorials and continue throughout the week. 
* Concerns: None
* Roadblocks: None

*Anna:*

* Accomplishments: Working the React.js tutorials and will conintue throughout the week. Was able to clone the NODE WEB repository from the gitlab. 
* Concerns: None
* Roadblocks: Caught on NPM stuff, but got past.

**No Meeting on 11/25 (Thanksgiving time)**

**Meeting 11/27:**

*Ben:*

* Accomplishments: Looked through documentation for terraform + firebase auth and began some initial code.
* Concerns: Firebase doesn’t seem to have much support for laC, so it may make set up a bit more difficult, especially without owner permissions myself.
* Roadblocks: Need Owner permissions to create new firebase project, so will need Casey Black to create it.

*Joey:*

* Accomplishments: Worked on the React.js tutorials and continue throughout the week. Going to clone repositories. 
* Concerns: None
* Roadblocks: None

*Anna:*

* Accomplishments: Continued learning about React via React’s documentation and Hess’s website
* Concerns: First version of login/create account might not be presentable given the time left in the sprint and the react learning curve.
* Roadblocks: None


**Meeting 11/30**

*Anna:*

* Accomplishments: Continued her react.js tutorials
* Concerns: First example isn’t gonna be great for the sprint.
* Roadblocks: None

*Joey:*

* Accomplishments: Continued the react.js and redux tutorials
* Concerns: First example isn’t gonna be great for the sprint.
* Roadblocks:

*Ben:*

* Accomplishments: Actually just setting up the sprint one review and getting his portfolio site updated mostly on the retrospective.
* Concerns: N/A
* Roadblocks: N/A
