**Meeting 4/19**

Ben: 
* Accomplishments: N/A
* Concerns: N/A
* Roadblocks: N/A

Anna: 
* Accomplishments: N/A just spent time trying to figure out firebase but made no real progress
* Concerns: finishing all of my tasks ASAP
* Roadblocks: struggling to find anything helpful about firebase auth online, running into issues with the way I've tried to set it up

Joey:
* Accomplishments: N/A
* Concerns: N/A
* Roadblocks: N/A


**Meeting 4/23**

Ben: 
* Accomplishments: Started work on portfolio and pitch individual assignments
* Concerns: N/A
* Roadblocks: N/A

Anna: 
* Accomplishments: Finished firebase auth integration (still need to add auth providers)
* Concerns: N/A
* Roadblocks: N/A

Joey:
* Accomplishments: Will work on fixing navbar for homepage, working on email template page
* Concerns: N/A
* Roadblocks: N/A


**Meeting 4/26**

Ben: 
* Accomplishments: Finished documentation, tests, and code comments for geoinfoprovider
* Concerns: N/A
* Roadblocks: N/A

Anna: 
* Accomplishments: Worked on offices and office results page, finishing up reset password.
* Concerns: N/A
* Roadblocks: Getting database updated

Joey:
* Accomplishments: Fixed navbar issue, continued work on email template page and changing to modal, began working on autocomplete.
* Concerns: Getting everything done by the end of the week.
* Roadblocks: Integrating autocomplete


**Meeting 4/30**

Ben: 
* Accomplishments: Completed tests, documentation, and comments for GraphQL and Office Data Manager. Fixed  error with creds folder on NODE Web during the docker build step
* Concerns: Hoping Vishnu doesn't ask for a lot more documentation, as I still need to add docs for some of the cloud code
* Roadblocks: N/A

Anna: 
* Accomplishments: Worked on touching up front end and wrapping things up. Finished firebase auth providers, but ran into a couple errors with some of them.
* Concerns: N/A
* Roadblocks: N/A

Joey:
* Accomplishments: N/A
* Concerns: N/A
* Roadblocks: N/A


**Meeting 5/3**

Ben: 
* Accomplishments: Started working on docs for infrastructure/cloud code
* Concerns: N/A
* Roadblocks: N/A

Anna: 
* Accomplishments: Will get started on comments and integrating real data
* Concerns: Creating automated tests
* Roadblocks: N/A

Joey:
* Accomplishments: N/A
* Concerns: N/A
* Roadblocks: N/A


**Meeting 5/7**

Ben: 
* Accomplishments: Finished docs for backend + cloud, helped Anna with deploying frontend, helped Joey with setting up maps API
* Concerns: N/A
* Roadblocks: N/A

Anna: 
* Accomplishments: Adding data to database
* Concerns: N/A
* Roadblocks: N/A

Joey:
* Accomplishments: Autocomplete, code comments, will work on adding layout stuff
* Concerns: N/A
* Roadblocks: Autocomplete API issue, getting photos to display
