**Meeting 1/25**

*Ben:*
* Accomplishments: Added GCR to terraform
* Concerns: None
* Roadblocks: None

*Joey:*
* Accomplishments: Worked with styling and tailwind, planning to get skeleton page for next Thursday
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Same as last meeting, aim to have login and create account page done by next Thursday
* Concerns: None
* Roadblocks: None

**Meeting 1/28**

*Ben:*
* Accomplishments: Gitlab push docker images to GCR, started setting up kubernetes to deploy images
* Concerns: None
* Roadblocks: Terraform provider difficulties

*Joey:*
* Accomplishments: Work on home page, changed navbar to be responsive for resizing, styling changes
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Will finish create account page today, created basic sign in page -- still need styling
* Concerns: None
* Roadblocks: None

**Meeting 1/29**

*Ben:*
* Accomplishments: Worked on presentation
* Concerns: None
* Roadblocks: None

*Joey:*
* Accomplishments: Worked on presentation
* Concerns: None
* Roadblocks: Linking with NextJS

*Anna:*
* Accomplishments: Worked on presentation
* Concerns: None
* Roadblocks: None

**Meeting 2/1**

*Ben:*
* Accomplishments: Was able to deploy kubernetes pod with office-data-manager api via Terraform for dev and prod
* Concerns: None
* Roadblocks: None

*Joey:*
* Accomplishments: Still working on skeleton homepage, error handling, will push navbar code by tomorrow
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Fixing linting errors
* Concerns: Incorporating navbar code from Joey
* Roadblocks: None

**Meeting 2/4**

*Ben:*
* Accomplishments: Got automated deployments for dev and prod up and running for the office data manager repo
* Concerns: Time
* Roadblocks: Had some issues getting kubectl working which took longer than I hoped, but those should be resolved now

*Joey:*
* Accomplishments: Almost finished up skeleton homepage, fixed linting errors and pushed to branch for Anna, pass string through Redux dispatch
* Concerns: Time
* Roadblocks: None

*Anna:*
* Accomplishments: 
    * fixed linting errors in my code (all from my previous commit)
   * made headway styling the login page
   * added a "sign in with google" button
* Concerns: None
* Roadblocks:
    * default type of the google sign in button is a 'button' but when I try to style it, the only thing I can change is the width (I'm not sure why the button is so big)
   * ran into an issue styling the antd components (which is why the email and password inputs are not styled) but found a source that explained it, which I will explore further tonight

**Meeting 2/5**

*Ben:*
* Accomplishments: Same as before
* Concerns: Time
* Roadblocks: Same as before

*Joey:*
* Accomplishments: Same as before
* Concerns: Time
* Roadblocks: None

*Anna:*
* Accomplishments: Close to being done with login
* Concerns: Time
* Roadblocks: Getting fonts to work

**Meeting 2/8**

*Ben:*
* Accomplishments: Completed NODE-19 automated deployments tickets.
* Concerns: None
* Roadblocks: None

*Joey:*
* Accomplishments: Made it possible to navigate in the navbar, finishing up most of skeleton homepage. Decided on Tailwind CSS component library.
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Finished most of login page template. Decided on Tailwind CSS component library.
* Concerns: None
* Roadblocks: None

**Meeting 2/11**

*Ben:*
* Accomplishments: Milestone 2
* Concerns: None
* Roadblocks: None

*Joey:*
* Accomplishments: 
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Password recovery page, milestone 2
* Concerns: None
* Roadblocks: None

**Meeting 2/12**

*Ben:*
* Accomplishments: 
* Concerns: None
* Roadblocks: None

*Joey:*
* Accomplishments: 
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: 
* Concerns: None
* Roadblocks: None
