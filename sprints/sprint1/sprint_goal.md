The goal of this sprint will be focused on the initial design and setup of the project.

Specifically, the design will entail:
* Designing the user interface
* Designing the backend and cloud architecture
* Designing the database
* Choosing the tech stack

The setup will involve:
* Setting up the cloud provider (servers, IAM roles, terraform configs, etc)
* Setting up automated deployments via CI/CD
* Completing initial setup for each repo
* Creating frontend and backend repos with starter code
* Creating terraform repos with initial server setup
* Creating repos with initial database setup
