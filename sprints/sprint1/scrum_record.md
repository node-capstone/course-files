**Meeting 11/2**

*Ben:*
* Accomplishments: Loaded census data into PostGIS and tested how an address could be queried.
* Concerns: Not sure how contributor alterations to census drawn districts will work in this model.
* Roadblocks: None

*Joey:*
* Accomplishments: Planned to design home and about page for next meeting.
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Planned to design office and login page for next meeting.
* Concerns: Unsure about differences in login for each role.
* Roadblocks: None




**Meeting 11/4**

*Ben:*
* Accomplishments: Created diagrams with pros/cons for several backend design options (incorporating address search only).
* Concerns: Still unsure of how best to handle community contributions.
* Roadblocks: Waiting for complete sample data to determine how database should be designed.

*Joey:*
* Accomplishments: Created home page designs. Incorporated about page as well.
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Created designs for office info page. Began on login page design.
* Concerns: None
* Roadblocks: Unsure how login will work for different user types.




**Meeting 11/6**

*Ben:*
* Accomplishments: Finished first draft cloud architecture, created rough database design.
* Concerns: None
* Roadblocks: Questions about DB requirements (how to handle address fields, meetings, candidates, etc).

*Joey:*
* Accomplishments: Completed UI/UX brainstorming document.
* Concerns: None
* Roadblocks: Was unsure on how some of the data would be stored.

*Anna:*
* Accomplishments: Completed detailed design for search, results, and login.
* Concerns: Questions about how community updated vs census boundaries would work.
* Roadblocks: None





**Meeting 11/9**

*Ben:*
* Accomplishments: Changed DB diagram for firestore.
* Concerns: None
* Roadblocks: Need GCP access to spin up servers.

*Joey:*
* Accomplishments: Continued working on mock ups. Will meet with Anna for UI later today.
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Added new screen ideas based on Jims user experience document. Will meet with Joey for UI later today.
* Concerns: None
* Roadblocks: None




**Meeting 11/11**

*Ben:*
* Accomplishments: Setup Google Cloud project. Setup NextJS repo for frontend with boilerplate code. Began setting up Terraform.
* Concerns: None
* Roadblocks: Don't have access to create IAM roles in GCP, so I need to be granted that role (already reached out on slack about this).

*Joey:*
* Accomplishments: Worked on higher fidelity designs with Anna. Home screen and account screens finished, nearly done with others.
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Worked on higher fidelity designs with Joey. Home screen and account screens finished, nearly done with others.
* Concerns: None
* Roadblocks: None




**Meeting 11/13**

*Ben:*
* Accomplishments: Completed basic initial cloud setup with Terraform. Automated Terraform verify, plan, and apply process in gitlab.
* Concerns: None
* Roadblocks: None

*Joey:*
* Accomplishments: Completed high fidelity interactive UI demo with Anna.
* Concerns: None
* Roadblocks: None

*Anna:*
* Accomplishments: Completed high fidelity interactive UI demo with Joey. Reached out to Professor Winters about open source license.
* Concerns: None
* Roadblocks: None