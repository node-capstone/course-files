**Meeting 2/15**

Ben:
* Accomplishments: N/A
* Concerns: Time/ Management of Class
* Roadblocks: N/A

Anna:
* Accomplishments: N/A, continued working on it
* Concerns: Time/ Class Management
* Roadblocks: N/A

Joey:
* Accomplishments: Installed Tailwind components and layout
* Concerns: N/A
* Roadblocks: N/A

**Meeting 2/18**

Ben:
* Accomplishments: Created OfficeDB repository , Set up office databases. Going to start creating some endpoints for the office data manager. 
* Concerns: Questions for frontend (me and Anna) how data will schedule
* Roadblocks: N/A

Anna:
* Accomplishments: Worked on layout from detailed search result pages.
* Concerns: confusion tailwind.
* Roadblocks: N/A

Joey:
* Accomplishments: Refactored Nav Bar and added global styles/colors for tailwind library.
* Concerns: 
* Roadblocks: 

**Meeting 2/19**

Ben:
* Accomplishments: Geo ID API creation! Next step Joey and Anna working with querying the API’s
* Concerns: N/A
* Roadblocks: N/A

Anna:
* Accomplishments: Got started on the front-end details view page.
* Concerns: N/A
* Roadblocks: N/A

Joey:
* Accomplishments: Not much haha
* Concerns: 
* Roadblocks: 

**Meeting 2/22**

Ben:
* Accomplishments: Added the meetings, office, and everything endpoints. The Backend is feature complete for this demo piece. Ben spent like all day doing this! What a king!
* Concerns: Skimping through a few development processes due to time and functionality desires.
* Roadblocks: N/A

Anna:
* Accomplishments: Refactoring branches and tickets, installed tailwind, trying to pass pipeline tests.
* Concerns: Time desired functionally 
* Roadblocks: N/A

Joey:
* Accomplishments: Refactored homepage with tailwind components.
* Concerns: N/A
* Roadblocks: N/A

**Meeting 2/25**

Ben:
* Accomplishments: No updates!
* Concerns: N/A
* Roadblocks: N/A

Anna:
* Accomplishments: Installed tailwind and refactored some code, she integrated tailwind to the search results page!
* Concerns: Firebase integration… next steps?
* Roadblocks: Tailwinds.

Joey:
* Accomplishments: Installed Apollo Client and created a few queries for the the search results page. Working on parsing the JSON data for display.
* Concerns:  N/A
* Roadblocks:  N/A

**Meeting 2/26**

Ben:
* Accomplishments:Same as last meeting
* Concerns: N/A
* Roadblocks: N/A

Anna:
* Accomplishments: Same as last meeting
* Concerns: N/A
* Roadblocks: N/A

Joey:
* Accomplishments: Same as last meeting
* Concerns: N/A
* Roadblocks: N/A

**Meeting 3/1/2020**

Ben:
* Accomplishments: N/A other classes
* Concerns: N/A
* Roadblocks:  N/A

Anna:
* Accomplishments: N/A other classes but mostly search details styling.
* Concerns: N/A
* Roadblocks: Tailwind toggle button issue

Joey:
* Accomplishments: Added search results and Search details functionality! 
* Concerns: 
* Roadblocks: 
