**Meeting 3/29**

Team didn't work over spring break, so there is nothing to report in terms of progress. 
We spent the meeting discussing our priorities and expectations for this sprint.


**Meeting 3/31**

* Team didn't meet today. Waiting to hear back from our TA to determine our weekly meeting schedule. 


**Meeting 4/02**

Ben: 
* Accomplishments: N/A
* Concerns: N/A
* Roadblocks: N/A

Anna: 
* Accomplishments: Began cleaning up styling for Login page (styled components --> tailwind), refactored login form using Formik
* Concerns: N/A
* Roadblocks: N/A

Joey:
* Accomplishments: Began cleaning up Home Page and NavBar, looking into Tailwind forms 
* Concerns: N/A
* Roadblocks: N/A


**Meeting 4/05**

Ben: 
* Accomplishments: worked on Terraform stuff (reset and reorganized), made scripts to automate processes, created Postgres database, enabled geolocation API
* Concerns: worried that GIS will be a pain to work with
* Roadblocks: N/A

Anna: 
* Accomplishments: Continued working on Create Account/Login pages (styling and refactoring)
* Concerns: Was concerned about duplicate files in our repo (tailwing configs), but less worried after discussing it with Joey
* Roadblocks: N/A

Joey:
* Accomplishments: Continued working on the NavBar and Homepage
* Concerns: worried about time, has an abnormally busy week for other classes so probably won't have much time to spare for this project until the weekend
* Roadblocks: N/A


**Meeting 4/07**

* Team decided to no longer meet on Wednesdays -- In addition to meeting with our project partner, Jim, we will be meeting with our TA on Fridays moving forward.


**Meeting 4/09**

Ben: 
* Accomplishments:
* Concerns:
* Roadblocks:

Anna: 
* Accomplishments:
* Concerns:
* Roadblocks:

Joey:
* Accomplishments:
* Concerns:
* Roadblocks:


**Meeting 4/12**

Ben: 
* Accomplishments: finished all of sprint 6 tasks; address searching is functional (geoids don't match up yet since data is fake) 
* Concerns: N/A
* Roadblocks: N/A

Anna: 
* Accomplishments: worked on polishing the search results page and search results detailed page
* Concerns: N/A
* Roadblocks: N/A

Joey:
* Accomplishments: working on NavBar (making it responsive and working on transition effects), exploring templates for the About Page
* Concerns: N/A
* Roadblocks: N/A


**Meeting 4/16**

Ben: 
* Accomplishments:
* Concerns:
* Roadblocks:

Anna: 
* Accomplishments:
* Concerns:
* Roadblocks:

Joey:
* Accomplishments:
* Concerns:
* Roadblocks:
